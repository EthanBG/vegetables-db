[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/garlic.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/clove-garlic_1759062?term=garlic&page=1&position=55&page=1&position=55&related_id=1759062&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "garlic",
  "name": "Garlic",
  "type": ["vegetable", "bulb"],
  "fromTree": false,
  "family": "Alliaceae",
  "synopsis": "Its culture, very simple, does not require particular attention and the species produced in France propose a broad range of tastes. In the kitchen, this condiment enhances and sublimates flavors, while contributing to your well-being thanks to its nutritional benefits.",
  "preservation": "6 months to 1 year in a dark place",
  "nutritional": "130 kcal for 100g",
  "vitamin": "Rich in vitamin B6",
  "history": "An everyday delight: The Greeks and Romans even made it their breakfast. The latter consisted of a nice slice of bread with a clove of garlic rubbed on its crumb. The Gallic people were also very fond of this condiment, a taste that continued in the Middle Ages.",
  "imagePath": "../../../assets/vegetables/icons/garlic.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 15,
      "spacingBtRows": 30
    },
    "monthOfPlanting": [10, 11],
    "monthOfHarvest": 7,
    "plantingInterval": null,
    "plantation": "Plant winter garlic in October-November and spring garlic from March to June. Plant the cloves with the tips up, 3 cm deep. The tip should be flush with the surface.",
    "harvest": "July for garlic planted in the fall, garlic planted in January gives three months later the carnation which looks like a new onion, June to July for white garlic.",
    "soil": "Not too compact, garlic prefers light soil. A sandy, slightly chalky soil may be suitable. Do not add compost or manure, which could rot the bulbs.",
    "tasks": "Some light hoeing.",
    "favourableAssociation": ["Carrot", "Cucumber", "Strawberry", "Onion", "Leek", "Tomato"],
    "unfavourableAssociation": ["Cabbage", "Bean"],
    "favourablePrecedents": ["Carrot", "Tomato", "Lettuce", "Beetroot", "Bean"],
    "unfavourablePrecedents": ["Onion", "Shallot"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/ail) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 102

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
