[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/strawberry.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://fr.freepik.com/icones-gratuites/fraise_15279328.htm)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "strawberry",
  "name": "Strawberry",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Rosaceae",
  "synopsis": "They bring color to our plates, tantalize our taste buds... and above all provide us with a good dose of vitamins and antioxidants.",
  "preservation": "48h max. in the refrigerator",
  "nutritional": "38 kcal for 100g",
  "vitamin": "Rich in vitamin C",
  "history": "Since the highest antiquity, it grows wild in America, Asia and Western Europe. The Romans used it to make beauty masks.",
  "imagePath": "../../../assets/vegetables/icons/strawberry.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 3,
      "coldResistance": 2,
      "rowSpacing": 40,
      "spacingBtRows": 50
    },
    "monthOfPlanting": [4, 5],
    "monthOfHarvest": [6, 10],
    "plantingInterval": 6,
    "plantation": "Opt for newly released climbing varieties. You will trellis them on a trellis with raffia.",
    "harvest": "From June to October (for remontant varieties)",
    "soil": "Cool, airy, moderately sunny.",
    "tasks": "Hoe frequently and mulch to maintain moisture. Water the soil abundantly as soon as the fruits are formed.",
    "favourableAssociation": ["Garlic", "Chives", "Shallot", "Spinach", "Bean", "Turnip", "Onion", "Leek"],
    "unfavourableAssociation": ["Cabbage"],
    "favourablePrecedents": ["Watercress"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/petits-fruits-et-fruits-rouges/fraise) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 114

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
