[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/courgette.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/courgette_700844?term=courgette&page=1&position=6&page=1&position=6&related_id=700844&origin=search)
### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "courgette",
  "name": "Courgette",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Cucurbitaceae",
  "synopsis": "This vegetable of the sun which smells good the Mediterranean is present in the plates all summer.",
  "preservation": "4 or 5 days in a cool and dry place",
  "nutritional": "23 kcal for 100g",
  "vitamin": null,
  "history": "Like many other fruits and vegetables, squash was discovered by Europeans when they landed in the New World. They quickly described the different varieties, before bringing them back to cultivate them as a vegetable.",
  "imagePath": "../../../assets/vegetables/icons/courgette.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 100,
      "spacingBtRows": 100
    },
    "monthOfPlanting": 5,
    "monthOfHarvest": [7, 10],
    "plantingInterval": null,
    "plantation": "Transplant mid-May, packing well around the base.",
    "harvest": "From July to October. Pick the zucchini when they reach about 20 cm for the tongue varieties, 8 to 10 cm in diameter for the round ones.",
    "soil": "Lightweight, potting soil",
    "tasks": "Water thoroughly without wetting the foliage. Bine regularly.",
    "favourableAssociation": ["Basil", "Onion", "Bean"],
    "unfavourableAssociation": ["Cucumber"],
    "favourablePrecedents": ["Cabbage"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/courges/courgette) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 227 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 59 and 111

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
