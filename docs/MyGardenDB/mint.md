[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/mint.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/mint_4543642?term=mint&page=1&position=7&page=1&position=7&related_id=4543642&origin=style)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "mint",
  "name": "Mint",
  "type": ["plant", null],
  "fromTree": false,
  "family": "Lamiaceae",
  "synopsis": "Mint is an aromatic perennial plant that is easy to grow and requires little maintenance. Indoors or outdoors, in a vegetable garden or on a balcony, mint adapts to all situations.",
  "preservation": "7 to 8 days in ambient air",
  "nutritional": null,
  "vitamin": null,
  "history": "The name \"mint\" comes from a nymph named Minthe or Menthe, a character in Greek mythology who, according to legend, was Pluto's girlfriend. Pluto's wife, Persephone, became jealous and turned Minthe into a ground-clinging plant. Although Pluto was unable to change Minthe back into a nymph, he gave her the ability to sweeten the air when her leaves and stems were crushed.",
  "imagePath": "../../../assets/vegetables/icons/mint.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 5,
    "monthOfHarvest": [6, 10],
    "plantingInterval": null,
    "plantation": "Mint is very invasive, to contain it plant each foot in a cylindrical box with a perforated bottom. Cut the stems in the fall. Renew the plants every 3 years and try different varieties which all have a distinctive scent.",
    "harvest": "Before flowering.",
    "soil": "The soil must be dense and shaded.",
    "tasks": "Watering should be copious.",
    "favourableAssociation": ["Broccoli"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- Book: "Potager en carrés" - Marabout 2011 page: 116

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
