[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/lettuce.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/laitue_4693154?term=laitue&page=1&position=20&page=1&position=20&related_id=4693154&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "lettuce",
  "name": "Lettuce",
  "type": ["vegetable", "leafy"],
  "fromTree": false,
  "family": "Asteraceae",
  "synopsis": "Crunchy or melting, fleshy or light, it is mainly eaten in salads, but lends itself to many cooked preparations.",
  "preservation": "1 to 2 days in the crisper",
  "nutritional": "14 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin K1",
  "history": "The Greeks and Romans cultivated lettuce of the Celtic or Roman type, which was not headed. They consumed it in particular to prepare their stomach for their copious meals.",
  "imagePath": "../../../assets/vegetables/icons/lettuce.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 2,
      "coldResistance": 1,
      "rowSpacing": 25,
      "spacingBtRows": 40
    },
    "monthOfPlanting": [5, 7],
    "monthOfHarvest": [7, 9],
    "plantingInterval": null,
    "plantation": "You can realize your seedlings in nursery or directly in place. You will need more seeds, but the seedlings will gain in precocity and will be more resistant to drought.",
    "harvest": "The harvest starts about 2 months after sowing",
    "soil": "Rather light, warming up quickly",
    "tasks": "When transplanting, do not bury the collar too much. Mulch",
    "favourableAssociation": ["Cabbage", "Carrot", "Cucumber", "Bean", "Tomato"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/salades/laitue) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 240 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 115

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
