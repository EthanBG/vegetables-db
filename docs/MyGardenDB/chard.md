[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/chard.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/chard_2771107?term=chard&page=1&position=6&page=1&position=6&related_id=2771107&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "chard",
  "name": "Chard",
  "type": ["vegetable", "leafy"],
  "fromTree": false,
  "family": "Chenopodiaceae",
  "synopsis": "Appearing on the shores of the Mediterranean, chard (also called Swiss chard) has been eaten since antiquity.",
  "preservation": "In the crisper ",
  "nutritional": "16 kcal for 100g",
  "vitamin": "Rich in vitamin k1",
  "history": "According to botanists and historians, the chard would have been born in the heat of the Mediterranean basin. Traces of cultivation and consumption have been found in Mesopotamia as well as in Ancient Rome. In the Middle Ages, it was eaten in a soup, the \"porée\" (or \"poirée\"), which was mixed with leeks. This rustic vegetable, also known as chard, is very popular with consumers, so much so that it is grown in large quantities. The species are selected to provide large ribs. In the 16th century, the botanist Bauhin listed varieties of different colors: white, green, but also yellow and red. At the end of the 19th century, about ten varieties were cultivated, but at the beginning of the following century, consumption declined little by little before being revived in the last decades.",
  "imagePath": "../../../assets/vegetables/icons/chard.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 30,
      "spacingBtRows": 40
    },
    "monthOfPlanting": 4,
    "monthOfHarvest": [6, 10],
    "plantingInterval": 4,
    "plantation": "Starting in April",
    "harvest": "from June to mid-October depending on the variety",
    "soil": "Cool",
    "tasks": "Water frequently. By mulching in winter, it happens that the feet start again the following spring.",
    "favourableAssociation": ["Garlic", "Celery", "Cabbage", "Onion"],
    "unfavourableAssociation": ["Carrot", "Tomato"],
    "favourablePrecedents": ["Spinach", "Lettuce", "Potato"],
    "unfavourablePrecedents": ["Bean"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/blette) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 207 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 104

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
