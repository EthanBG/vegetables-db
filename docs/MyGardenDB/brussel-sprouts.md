[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/brussel-sprouts.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: <Author name>](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/brussel-sprouts_3309403?term=brussel-sprouts&page=1&position=27&page=1&position=27&related_id=3309403&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "brussel-sprouts",
  "name": "Brussel-Sprouts",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Young chefs have understood that Brussels sprouts are no longer exclusively a symbol of traditional rustic food, but can be used in a variety of modern ways.",
  "preservation": "7 days in the crisper",
  "nutritional": "45 kcal for 100g",
  "vitamin": "Rich in vitamin K1 and C1",
  "history": "In the 17th century, in order to optimize their yield, the market gardeners of Saint-Gilles created a new variety of cabbage that was grown vertically and thus took up less space.",
  "imagePath": "../../../assets/vegetables/icons/brussel-sprouts.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 3,
      "coldResistance": 1,
      "rowSpacing": 50,
      "spacingBtRows": 70
    },
    "monthOfPlanting": [3, 6],
    "monthOfHarvest": [9, 3],
    "plantingInterval": null,
    "plantation": "Since the plants grow tall, staking them is recommended.",
    "harvest": "The base apples are harvested first, as soon as they reach a diameter of 2-3 cm. The first frosts improve the taste quality of semi-late and late varieties.",
    "soil": null,
    "tasks": null,
    "favourableAssociation": ["Spinach", "Lettuce", "Potato", "Courgette"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Courgette", "Potato"],
    "unfavourablePrecedents": ["Carrot", "Celery", "Bean", "Tomato", "Corn", "Melon"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/choux/choux-de-bruxelles) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 216 - FR
- ["www.jardiner-malin.fr"](https://www.jardiner-malin.fr/fiche/chou-de-bruxelles.html) - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
* 
