[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/chives.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/chives_6969402?term=chives&page=1&position=55&page=1&position=55&related_id=6969402&origin=style)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "chives",
  "name": "Chives",
  "type": ["herb", null],
  "fromTree": false,
  "family": "Alliaceae",
  "synopsis": "The chive, also called \"Fines herbes\", is an aromatic plant whose fine and elongated leaves are consumed in salads, omelets, white cheeses...",
  "preservation": null,
  "nutritional": null,
  "vitamin": null,
  "history": "The Chinese have been growing and cooking with garlic chives for at least 3,000 years, dating back to the Chou dynasty (1027 BC to 256 BC). But the popularity of this herb extends beyond China. Japanese cooks call garlic chives nira and use them frequently in meat and seafood recipes.",
  "imagePath": "../../../assets/vegetables/icons/chives.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 2,
      "coldResistance": 1,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 3,
    "monthOfHarvest": [4, 11],
    "plantingInterval": null,
    "plantation": "Transplant in March. Plant the bulbs 1 cm below ground.",
    "harvest": "April to November, but you can harvest this perennial most of the year if you protect it from the cold.",
    "soil": "Fertile, but chives are not very demanding.",
    "tasks": "Water after each cut.",
    "favourableAssociation": ["Carrot"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 220 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 110
- [Thespruceeats](https://www.thespruceeats.com/what-are-garlic-chives-695295)


### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
