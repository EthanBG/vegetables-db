[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/turnip.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/diet_1759153?term=turnip&page=1&position=83&origin=search&related_id=1759153)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "turnip",
  "name": "Turnip",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Originally from Eastern Europe, this watery root is not as bland as its reputation might suggest. On the contrary, its distinctive taste lends a much-needed touch to many regional specialties and highly refined dishes.",
  "preservation": "3 or 4 days in the crisper",
  "nutritional": "21 kcal per 100g",
  "vitamin": "Rich in vitamins C and B9",
  "history": "For a long time a staple of popular and rural diets, the turnip has gradually become a diversification vegetable. Now, step by step, it's making a comeback in gourmet cuisine.",
  "imagePath": "../../../assets/vegetables/icons/turnip.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 2,
      "coldResistance": 2,
      "rowSpacing": 7,
      "spacingBtRows": 20
    },
    "monthOfPlanting": [3, 6],
    "monthOfHarvest": [5, 8],
    "plantingInterval": 2,
    "plantation": "Sow spring-summer turnips between March and June. For autumn-winter cultivation, sow in July-August and water regularly to keep flea beetles at bay. As the root matures, watering should be sustained to ensure that the turnip doesn't get too strong a taste and remains firm.",
    "harvest": "Turnips can be harvested quickly, about two months after sowing. Its taste is sweeter when harvested young. At the start of winter, you can take all the turnips out of the ground to protect them from frost. Store them in a cool, dark place.",
    "soil": "Cool and light.",
    "tasks": "Hoe, weed and water. Turnips fear summer drought.",
    "favourableAssociation": ["Carrot", "Spinach", "Borage", "Dill", "Chard", "Endive"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": ["Courgette"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

There are a few contradictions between good and bad associations in the book: Ma bible de la permaculture.

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/navet) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 248 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 117
### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **Adrien DERACHE** - 2023 - a.d44@tuta.io
