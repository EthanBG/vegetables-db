[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/lamb-lettuce.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/mache_6025959?term=mache&page=1&position=4&page=1&position=4&related_id=6025959&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "lamb-lettuce",
  "name": "Lamb's lettuce",
  "type": ["vegetable", "leafy"],
  "fromTree": false,
  "family": "Valerianaceae",
  "synopsis": "These round rosettes of leaves, of a beautiful dark green and shiny, are fleshy and crunchy. Originally from the Mediterranean basin, lamb's lettuce is the standard winter salad on the shelves, although it can be eaten all year round.",
  "preservation": "2 or 3 days in the fridge",
  "nutritional": "16 kcal for 100g",
  "vitamin": "Rich in vitamin k1",
  "history": "On retrouve la mâche représentée sur les tombeaux égyptiens, dès le Ve siècle avant notre ère. En France, elle est cultivée depuis le XVIe siècle, déjà sur les bords de Loire. Malgré son goût subtil, elle reste longtemps une nourriture paysanne et rustique.",
  "imagePath": "../../../assets/vegetables/icons/lamb-lettuce.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 1,
      "coldResistance": 2,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 9,
    "monthOfHarvest": [9, 11],
    "plantingInterval": null,
    "plantation": "Sow in place from August 1st in slightly cool places. You can even sow it at the foot of a cauliflower without digging the soil. Bury the seed in the soil and pack it down well.",
    "harvest": "Start by harvesting the best rosettes, and let the ones next to them grow.",
    "soil": "A bit clayey",
    "tasks": "Remove the leaves from the base that would tend to yellow. Mulch.",
    "favourableAssociation": ["Celery", "Bean", "Leek", "Strawberry", "Lettuce", "Onion"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/salades/mache/tout-savoir-sur-la-mache) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 242 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 116

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
