﻿[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: http://127.0.0.1:8000/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/jsweet_potato.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik]((https://www.freepik.com)
- [image info](https://www.flaticon.com/free-icon/sweet-potato_4179126?term=sweet+potato&page=1&position=96&origin=style&related_id=4179126)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json
{
  "id": "sweet_potato",
  "name": "Sweet Potato",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Convolvulaceae",
  "synopsis": "The sweet potato is a tuber requiring more heat than the potato. It is easy to grow in the South of France and by the sea. In cooler climates, it is preferable to place it in a greenhouse.",
  "preservation": null,
  "nutritional": "79 kcal for 100g",
  "vitamin": "Rich in vitamin A, B6, B9 and C",
  "history": "The origin and domestication of sweet potato occurred in either Central or South America. In Central America, domesticated sweet potatoes were present at least 5,000 years ago, with the origin of I. batatas possibly between the Yucatán Peninsula of Mexico and the mouth of the Orinoco River in Venezuela.",
  "imagePath": "../../../assets/vegetables/icons/sweet_potato.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 1,
      "coldResistance": -1,
      "rowSpacing": 40,
      "spacingBtRows": 40
    },
    "monthOfPlanting": 3,
    "monthOfHarvest": [8,11],
    "plantingInterval": null,
    "plantation": "Germinate the tubers from the month of March in pots filled with compost / potting soil. You will place them in the ground in April-May, when the risk of frost has passed.",
    "harvest": "Harvest before frost.",
    "soil": "You should use potting soil",
    "tasks": null,
    "favourableAssociation": null,
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/patate-douce) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 254 - FR


### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **Emilio** - 2023 - ebriand@et.esiea.fr
