[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (LINKS :)
[//]: # (- Documentation: https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/)
[//]: # (- Need help ?: https://discord.gg/XPNBsarq8t)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/arugula.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/arugula_7931706?term=arugula&page=1&position=33&origin=search&related_id=7931706)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "arugula",
  "name": "Arugula",
  "type": ["vegetable", "leafy"],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Common arugula is a hardy lettuce that's very easy to grow (its cousin, wild arugula, is also widespread). Seeds germinate very quickly, in less than a week.",
  "preservation": null,
  "nutritional": null,
  "vitamin": null,
  "history": "Grown as an edible and popular herb in Italy since Roman times, arugula was mentioned by various ancient Roman classical authors as an aphrodisiac, most famously in a poem long ascribed to the famous first century Roman poet Virgil, Moretum.",
  "imagePath": "../../../assets/vegetables/icons/arugula.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 10,
      "spacingBtRows": 30
    },
    "monthOfPlanting": [3, 4],
    "monthOfHarvest": 5,
    "plantingInterval": null,
    "plantation": "Sow in the field in March-April and thin out to leave one plant every 8 to 10 cm or so. Stagger sowing to enjoy several harvests.",
    "harvest": "May. Allow about 6 weeks between sowing and first harvest. Cut leaves as required.",
    "soil": "Rich, arugula tolerates shade.",
    "tasks": "Water regularly, but not excessively, without wetting the foliage.",
    "favourableAssociation": ["Cabbage"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 270 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 121
- [Wikipedia](https://en.wikipedia.org/wiki/Eruca_vesicaria)

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
