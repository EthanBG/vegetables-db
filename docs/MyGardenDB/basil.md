[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/basil.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/premium-icon/basil_4074264?term=basil&page=1&position=64&page=1&position=64&related_id=4074264&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "basil",
  "name": "Basil",
  "type": ["herb", null],
  "fromTree": false,
  "family": "Lamiaceae",
  "synopsis": "Basil is a staple of Mediterranean cuisine, whose Provençal name, pistou, is well known for its famous soup of which it is the basic aromatic.",
  "preservation": null,
  "nutritional": null,
  "vitamin": null,
  "history": "Basil is called by many names like sweet basil or even Thai basil, but all of its common names refer to the herb's botanical name, Ocimum basilicum. Basil is a member of the large mint family, or Lamiaceae family, along with other culinary herbs like rosemary, sage, and even lavender. It is believed that basil has origins in India, but the herb has been cultivated for over 5,000 with its reach spreading to all corners of the globe. There are some indications that basil may have originated even farther east than India with ancient records from 807 A.D. suggesting that sweet basil was used in the Hunan region of China at that time. Basil eventually migrated westward as whole plants as it could be grown easily indoors and away from exposure to cold climates and frost.",
  "imagePath": "../../../assets/vegetables/icons/basil.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 2,
    "monthOfHarvest": 6,
    "plantingInterval": null,
    "plantation": "From February (under frame) or transplant from May to July.",
    "harvest": "From June to mid-October depending on the variety",
    "soil": "Fresh and light",
    "tasks": "Water copiously, when the soil has cooled, in the evening or early morning. Harvest the leaves as needed.",
    "favourableAssociation": ["Cucumber", "Courgette", "Fennel"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 203 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 58 and 103
- [Thespruceeats](https://www.thespruceeats.com/the-history-of-basil-1807566)

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2023 - a.d44@tuta.io
