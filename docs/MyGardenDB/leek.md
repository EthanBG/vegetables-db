[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/leek.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/free-icon/leek_700846?term=leek&page=1&position=12&page=1&position=12&related_id=700846&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "leek",
  "name": "Leek",
  "type": ["vegetable", "stem"],
  "fromTree": false,
  "family": "Alliaceae",
  "synopsis": "A hardy vegetable, it is used in many dishes thanks to its distinctive aroma and garlicky flavor. Originating from the Middle East, it has crossed the centuries to be today the ninth most consumed vegetable by the French.",
  "preservation": "5 days in the crisper",
  "nutritional": "27 kcal for 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "The leek would come from the Middle East but its precise origins remain very vague. It was already cultivated in ancient Egypt where it symbolized victory: the pharaoh Kheops offered it to his best warriors.",
  "imagePath": "../../../assets/vegetables/icons/leek.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": 2,
      "rowSpacing": 15,
      "spacingBtRows": 30
    },
    "monthOfPlanting": 5,
    "monthOfHarvest": [7,9],
    "plantingInterval": 5,
    "plantation": "In May, before transplanting the plants, \"dress them up\" by cutting off half the roots and the tips of the leaves. Water the soil if it is dry.",
    "harvest": "From July to September",
    "soil": "Deep, rich and a bit cool.",
    "tasks": "Be careful not to create an air pocket around the roots by packing well when transplanting.",
    "favourableAssociation": ["Garlic", "Beetroot", "Carrot", "Celery", "Spinach", "Strawberry"],
    "unfavourableAssociation": ["Broad bean"],
    "favourablePrecedents": ["Carrot", "Watercress", "Lettuce", "Radish"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/poireau) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 60 and 120

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
