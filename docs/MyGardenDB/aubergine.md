[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/aubergine.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/aubergine_2754026?term=aubergine&page=1&position=18&page=1&position=18&related_id=2754026&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "aubergine",
  "name": "Aubergine",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Solanaceae",
  "synopsis": "The most frequent on the shelves is the purple eggplant, with its white and soft flesh.",
  "preservation": "Up to 5 days in the crisper",
  "nutritional": "33 kcal per 100g",
  "vitamin": "Rich in fiber and vitamin B5",
  "history": "The cultivation of eggplant is very old. It dates back to 800 BC, in the Indo-Burmese region. The vegetable still thrives in India, which has an impressive quantity of varieties of all colors. It is also found in China, four centuries before our era. The eggplant is then implanted in the Mediterranean basin thanks to the Arab navigators who bring it back from their voyages from the end of the world. The vegetable quickly acclimatized to the mild heat of the region, and quickly flourished. Little by little, the eggplant made its first steps on the European continent, cultivated by the Spaniards in the Middle Ages. It was not until the 15th century that it developed in Italy and in the south of France.",
  "imagePath": "../../../assets/vegetables/icons/aubergine.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 60,
      "spacingBtRows": 70
    },
    "monthOfPlanting": [3, 4],
    "monthOfHarvest": [7, 10],
    "plantingInterval": null,
    "plantation": "Sow in March/April, transplant at the end of May, when the plants have 5 to 6 leaves.",
    "harvest": "The fruits do not need to ripen: they can be cut as soon as they have reached their average size, using secateurs to avoid tearing the stem.",
    "soil": "Rich in humus and well drained..",
    "tasks": "Remove yellowing leaves",
    "favourableAssociation": ["Cucumber"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": ["Tomato"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-fruits/aubergine) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 201 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 103

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
