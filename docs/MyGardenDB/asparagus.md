[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/asparagus.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](<https://www.flaticon.com/free-icon/asparagus_1155298?term=asparagus&page=1&position=75&page=1&position=75&related_id=1155298&origin=search>)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the name in lower case without space and underscore)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "asparagus",
  "name": "Asparagus",
  "type": ["vegetable", "stem"],
  "fromTree": false,
  "family": "Liliaceae",
  "synopsis": "White, purple or green, asparagus is a springtime delight not to be missed!",
  "preservation": "3 days in the crisper",
  "nutritional": "18 kcal per 100g (white asparagus) 26 kcal per 100g (green asparagus)",
  "vitamin": "Rich in vitamin B9 (white asparagus) Rich in vitamin B9 and K1 (green asparagus)",
  "history": "Probably originating from the Mediterranean basin, asparagus was first eaten - most often in the wild - by the Egyptians and Greeks. It was the Romans who later developed its cultivation, but this vegetable was then reserved for rich gourmets.",
  "imagePath": "../../../assets/vegetables/icons/asparagus.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 2,
      "rowSpacing": 50,
      "spacingBtRows": 150
    },
    "monthOfPlanting": [2, 4],
    "monthOfHarvest": [4, 6],
    "plantingInterval": null,
    "plantation": "The \"claws\", which are the root trays from which the turions (the young edible stem) will grow, are placed at the bottom of a 25 cm trench, every 50 cm on the row. Three years after the placement of the claws, mounds of 25 to 30 cm are put up on the row. It is the beginning of the production, which spreads out over 2 or 3 months, from the end of March to June according to the regions. The plantation of asparagus can remain in place a dozen years.",
    "harvest": "Asparagus is harvested with a gouge, to cut each spear at its base without having to dig into the mound. The first year of production, it is necessary to limit the harvest to 1 month maximum to allow the plants to make reserves and the following years, one stops the takings at June 21. The foliage is then left to develop and will be cut only at the beginning of the winter, once yellowed.",
    "soil": "In the fall, to save space in the garden, sow lamb's lettuce on the mounds covering your asparagus.",
    "tasks": null,
    "favourableAssociation": null,
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Other information

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/asperge) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 199 - FR
- ["jardiner-malin"](https://www.jardiner-malin.fr/fiche/asperge.html) - FR
- ["saisons-fruits-legumes"](https://www.saisons-fruits-legumes.fr/legumes/asperge-blanche) - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
