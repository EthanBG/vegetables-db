[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/broccoli.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/broccoli_15471354.htm)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "broccoli",
  "name": "Broccoli",
  "type": ["fruit", null],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "This great relative of cauliflower has been present for a few decades on French shelves, and has quickly found its place on our plates. Its very mild taste and crunchy texture work wonders in all types of savory recipes. Packed with vitamins, this green vegetable also contributes to your well-being!",
  "preservation": "Between 4 and 5 days in the crisper.",
  "nutritional": "37 kcal for 100g",
  "vitamin": "Rich in vitamin B9 and K1",
  "history": "Originally from Italy, broccoli is a distant descendant of wild cabbage and cauliflower. And like its ancestors, only the apple is eaten. Created by the Romans, broccoli was developed from the most beautiful specimens of wild cabbage; it is even the intermediate step before the discovery of cauliflower.",
  "imagePath": "../../../assets/vegetables/icons/broccoli.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": null,
      "waterNeeds": null,
      "coldResistance": null,
      "rowSpacing": null,
      "spacingBtRows": null
    },
    "monthOfPlanting": 5,
    "monthOfHarvest": [8,9],
    "plantingInterval": null,
    "plantation": "Choose early varieties. Transplant in May.",
    "harvest": "August to September",
    "soil": "Fertile and fresh",
    "tasks": "Water regularly at the foot in dry weather. About one month after transplanting, hoe the plants up to the base of the first leaves. Hoe regularly.",
    "favourableAssociation": ["Dill", "Beetroot", "Camomile", "Nasturtium", "Celery", "Cucumber", "Gherkin", "Mint", "Oregano"],
    "unfavourableAssociation": ["Strawberry", "Lettuce"],
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/choux/brocoli) - FR
- Book: "Potager en carrés" - Marabout 2011 page: 105

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
