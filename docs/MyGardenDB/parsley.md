[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/parsley.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/persil_1155270?term=persil&page=1&position=13&page=1&position=13&related_id=1155270&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "parsley",
  "name": "Parsley",
  "type": ["plant", null],
  "fromTree": false,
  "family": "Apiaceae",
  "synopsis": "Both a condiment and a garnish, it brings its freshness and fragrance to many preparations.",
  "preservation": null,
  "nutritional": "38 kcal for 100g",
  "vitamin": "Rich in fiber and vitamin K1",
  "history": "In the Age of Enlightenment, parsley was attributed a thousand virtues. It cured almost everything!",
  "imagePath": "../../../assets/vegetables/icons/parsley.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 2,
      "coldResistance": 1,
      "rowSpacing": 2,
      "spacingBtRows": 20
    },
    "monthOfPlanting": [2,3],
    "monthOfHarvest": 7,
    "plantingInterval": null,
    "plantation": "Sow in rows from February-March until August. Let the seeds dry: they will germinate better. Thin out the plants when they have 2 or 3 leaves and leave 5 to 8 cm between each plant.",
    "harvest": "Starting in July",
    "soil": "Light and cool, preferably in the shade.",
    "tasks": "Cover the seedling with 1.5 cm of potting soil and pack lightly. Add soil to the feet during the culture.",
    "favourableAssociation": ["Tomato"],
    "unfavourableAssociation": ["Lettuce"],
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/aromatiques-fraiches/persil) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 256 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 119

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
