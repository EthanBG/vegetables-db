[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/fennel.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.flaticon.com/fr/icone-gratuite/fenouil_6969382?term=fenouil&page=1&position=11&page=1&position=11&related_id=6969382&origin=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "fennel",
  "name": "Fennel",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Apiaceae",
  "synopsis": "A symbolic and mythological reference since Antiquity, it is above all a particularly interesting food on the nutritional level. It can be found with pleasure in savory dishes, but also in sweet desserts.",
  "preservation": "A week in the crisper",
  "nutritional": "21 kcal for 100g",
  "vitamin": "Rich in vitamin k1 and B9",
  "history": "Sweet fennel began to be cultivated in Tuscany in the late Middle Ages. Popularized by Catherine de Medici, it became one of the favorite vegetables of Italians.",
  "imagePath": "../../../assets/vegetables/icons/fennel.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 20,
      "spacingBtRows": 40
    },
    "monthOfPlanting": [7, 8],
    "monthOfHarvest": [10, 11],
    "plantingInterval": null,
    "plantation": "Replant in July-August",
    "harvest": "Late October-early November",
    "soil": "In light, fresh soil with added sand. Warm exposure.",
    "tasks": "Water thoroughly and mulch if necessary.",
    "favourableAssociation": ["Celery"],
    "unfavourableAssociation": null,
    "favourablePrecedents": null,
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-feuilles/celeri-branche) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 231 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 113

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
