[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/potato.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/potato_14895888.htm#query=potatoes&from_query=pomme%20de%20terre&position=26&from_view=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "potato",
  "name": "Potato",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Solanaceae",
  "synopsis": "Whether you like them fried, au gratin, mashed or steamed and topped with melting butter, tasty potatoes are one of the pillars of the vegetable garden.",
  "preservation": null,
  "nutritional": null,
  "vitamin": "Rich in vitamin B6",
  "history": "Born 8,000 years ago on the plateaus of the Andes, the potato was quickly cultivated by the Incas in the 13th century. It was not until 3 centuries later that it reached European shores, thanks to the Spanish conquistadors. The vegetable then migrated to Italy, then to Germany and the south of France. It began to be cultivated under the impulse of Olivier de Serres and Charles de l'Escluze.",
  "imagePath": "../../../assets/vegetables/icons/potato.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 2,
      "waterNeeds": 2,
      "coldResistance": -1,
      "rowSpacing": 35,
      "spacingBtRows": 50
    },
    "monthOfPlanting": [1, 5],
    "monthOfHarvest": [4, 7],
    "plantingInterval": null,
    "plantation": "Depending on their earliness, potatoes can be planted from February to May. Traditionally, to avoid the development of weeds and to keep the tubers in the dark, the potatoes are ridged, bringing soil to the foot of each plant. The watering should be copious at the time of the formation and the enlargement of the tubers.",
    "harvest": "Potatoes are easy to store in a cool, dark place. However, it is necessary to degerminate the tubers two or three times if the storage temperature is not cool enough (ideally between 4 and 8 ℃), especially for varieties harvested in early summer.",
    "soil": null,
    "tasks": "Water regularly",
    "favourableAssociation": ["Pea"],
    "unfavourableAssociation": null,
    "favourablePrecedents": ["Cabbage"],
    "unfavourablePrecedents": ["Aubergine"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/pomme-de-terre) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 266 - FR

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
