[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)
[//]: # (Everything must be written in English, it will be translated later !)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/radish.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/radish_14848117.htm#query=radish&position=35&from_view=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[//]: # (The "id" is the first four letters of the name in lower case)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "radish",
  "name": "Radish",
  "type": ["vegetable", "tuber/root"],
  "fromTree": false,
  "family": "Brassicaceae",
  "synopsis": "Long, round, small or large, the different varieties offer a subtle palette of flavors. Crunched naturally with a little salt, it is a pure pleasure food recognized also for its nutritional qualities.",
  "preservation": "The same day",
  "nutritional": "14 kcal for 100g",
  "vitamin": "Rich in vitamin B9",
  "history": "Probably originating from Asia Minor, the radish was already on the menu of the Babylonians and Egyptians 4,000 years ago. Among the Greeks, the radish was dedicated to Apollo. The god sometimes received golden radishes as offerings.",
  "imagePath": "../../../assets/vegetables/icons/radish.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 0,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 2,
      "spacingBtRows": 15
    },
    "monthOfPlanting": [4, 5],
    "monthOfHarvest": [5, 6],
    "plantingInterval": null,
    "plantation": "Sow fairly lightly in April-May. Bury the seeds at 2 or 3 cm, soil if it is hot. Pack the soil lightly and water with the watering can with its apple. As soon as the plants have 3 or 4 leaves, thin them out leaving 5 cm between each plant.",
    "harvest": "3 to 4 weeks after sowing depending on the variety",
    "soil": "Rich deep and sandy",
    "tasks": "Hoe and water regularly. Weed by hand.",
    "favourableAssociation": ["Carrot", "Tomato"],
    "unfavourableAssociation": ["Cucumber"],
    "favourablePrecedents": null,
    "unfavourablePrecedents": ["Cabbage", "Turnip"]
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/radis) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 268 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 61 and 121

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
