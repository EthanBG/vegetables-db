[//]: # (---)
[//]: # (tags:)
[//]: # (- ⚠ Need review ⚠)
[//]: # (---)

[//]: # (Thank you very much for your contribution to the open source world!)

[//]: # (If the JSON is not complete or if there are problems, uncomment the 4 lines at the top.)

[//]: # (To add a vegetable please follow the next steps)

[//]: # (1. Replace <vegetable-name> by the vegetables name in lowercase)
[//]: # (2. download the vegetable icon in the docs/assets/icons/ directory)
[//]: # (The image will not be visible during the development process)
<figure markdown>
  ![Image title](https://gitlab.com/m9712/vegetables-db/-/raw/main/docs/assets/icons/onion.png){ width="150" }
</figure>

[//]: # (3. Replace <Author name> and <Author-website>)
[//]: # (4. Replace <image-url> by the link where you found the image)
- [Author: Freepik](https://www.freepik.com/)
- [image info](https://www.freepik.com/free-icon/spring-onion_15481344.htm#query=onion&position=3&from_view=search)

### JSON

[//]: # (https://m9712.gitlab.io/vegetables-db/Getting%20started/add-vegetables/ for more information)
[//]: # (Please, replace <*> by the correct information)
[jsonStart]:  # (Please don't remove this line)
```json linenums="1"
{
  "id": "onion",
  "name": "Onion",
  "type": ["vegetable", "bulb"],
  "fromTree": false,
  "family": "Allioideae",
  "synopsis": "Involved in an impressive number of preparations, it lends itself with as much success to the most rustic dishes (flamiche, gratinée...) as to the most sophisticated dishes (pan-fried foie gras, duck breast...). This bulb, an organ of nutritive reserves for the plant and its flower, also presents nutritional qualities which invite you to conjugate well-being and pleasure of the table.",
  "preservation": "3 or 4 days in the crisper",
  "nutritional": "21 kcal per 100g",
  "vitamin": "Rich in fiber and vitamin C",
  "history": "If we do not know the wild ancestor of the onion, we know on the other hand that it has been cultivated for more than 5000 years. The first traces of production come from Mesopotamia, its cultivation then reaching ancient Egypt (texts dating back more than 4,000 years mention it), Greece, the Roman Empire and then the rest of Europe and the World. It is even a symbol of intelligence in ancient China.",
  "imagePath": "../../../assets/vegetables/icons/onion.png",
  "cultureSheets": {
    "quickInfos": {
      "compostNeed": 1,
      "waterNeeds": 1,
      "coldResistance": 1,
      "rowSpacing": 15,
      "spacingBtRows": 30
    },
    "monthOfPlanting": 3,
    "monthOfHarvest": [7, 8],
    "plantingInterval": 3,
    "plantation": "Plant the small bulbs in March. Cut the roots at about 1 cm from the bulb and level the leaves. Bury each bulb at a maximum depth of 2 or 3 cm.",
    "harvest": "July and August",
    "soil": "Soft and light, not too humid.",
    "tasks": "Onions fear competition, so weed regularly.",
    "favourableAssociation": ["Carrot", "Lettuce", "Strawberry"],
    "unfavourableAssociation": ["Bean", "Leek", "Cabbage"],
    "favourablePrecedents": ["Radish", "Lettuce"],
    "unfavourablePrecedents": null
  }
}
```
[jsonEnd]: # (Please don't remove this line)

### Sources

[//]: # (Please list all sources used)
- [lesfruitsetlegumesfrais.fr](https://www.lesfruitsetlegumesfrais.com/fruits-legumes/legumes-racines-tubercules-et-tiges/oignon) - FR
- ["Ma bible de la permaculture"](https://www.editionsleduc.com/produit/2087/9791028516901/ma-bible-de-la-permaculture) by BLAISE LECLERC page: 250 - FR
- Book: "Potager en carrés" - Marabout 2011 page: 60 and 117

### Contributors

[//]: # (Replace <contributor-name> by your name)
[//]: # (If you modify the file you can put your name at the top of the list)

* **DERACHE Adrien** - 2022 - a.d44@tuta.io
