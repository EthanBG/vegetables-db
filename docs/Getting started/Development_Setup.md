MyGarden is an app built with [Ionic](https://ionicframework.com/), [Angular](https://angular.io/) and [Capacitor](https://capacitorjs.com/).

## Download Required Tools

Download and install these right away to ensure an optimal Ionic development experience:

- **Node.js** for interacting with the Ionic ecosystem. Download the LTS version [here](https://nodejs.org/en/).
- **A code editor** for... writing code! We are fans of Visual Studio Code.
- **Command-line interface/terminal (CLI)**:

    * **Windows** users: for the best Ionic experience, we recommend the built-in command line (cmd) or the Powershell CLI, running in Administrator mode.
    * **Mac/Linux** users, virtually any terminal will work.

## Install Node.js/npm

=== "Debian/Ubuntu/Linux Mint"

    ``` bash
    sudo apt install nodejs npm
    ```

=== "Fedora"

    ``` bash
    sudo dnf install nodejs npm
    ```
=== "Arch Linux"

    ``` bash
    sudo pacman -S npm
    ```
=== "OpenSUSE"

    ``` bash
    sudo zypper install npm
    ```
To make sure that npm has been installed successfully, run the following command:
```
npm --version
8.5.0
```
## Install the Ionic CLI

Install the Ionic CLI with npm:

```shell
sudo npm install -g @ionic/cli
```

## Get MyGarden files

Clone the [repository](https://gitlab.com/m9712/mygarden):
```shell
git clone https://gitlab.com/m9712/mygarden.git
```

```shell
cd mygarden/
```
## Run the App

Install the dependency:
```shell
npm install
```

Run this command next:
```shell
ionic serve
```
Your Ionic app is now running in a web browser at [localhost:8100](http://localhost:8100). Most of your app can be built and tested right in the browser, greatly increasing development and testing speed.

## Build for Android (For development only)
First follow this [guide](https://ionicframework.com/docs/developing/android) to install and configure Android Studio.

Run these commands next:

```shell
ionic capacitor add android
```
`ionic capacitor add` will do the following:

- Install the Capacitor platform package
- Copy the native platform template into your project

```shell
ionic capacitor copy android
```
`ionic capacitor copy` will do the following:

- Perform an Ionic build, which compiles web assets
- Copy web assets to Capacitor native platform(s)

```shell
ionic capacitor sync android
```
`ionic capacitor sync` will do the following:
    
- Perform an Ionic build, which compiles web assets
- Copy web assets to Capacitor native platform(s)
- Update Capacitor native platform(s) and dependencies
- Install any discovered Capacitor or Cordova plugins

```shell
ionic capacitor open android
```
`ionic capacitor open android` will do the following:
    
- Open Android Studio

Now, you can run the app on your real device or on a virtual device.
