### Types

#### Stable

Play store and f-droid releases for the masses. Pull Requests that have been tested and reviewed can go to master.
After the last release candidate is out in the wild for ~2 weeks and no errors get reported (by users or in the developer console) the master branch is ready for the stable release.
So when we decide to go for a new release we freeze the master feature wise.

#### Release Candidate

stable beta releases done via the Beta program of the Google Play store and f-droid.
Whenever a PR is reviewed/approved we put it on master.
Before releasing a new stable version there is at least one release candidate.
It is based on the current master and during this phase the master is feature freezed.
After ~2 weeks with no error a stable version will be released, which is identical to the latest release candidate.

#### Dev

Done as a standalone app that can be installed in parallel to the stable app.
Any PR which is labelled "ready for dev" will be automatically included in the dev app.
This label should only set by the main developers. Same applies for the android-library.
This repository also has a branch called dev which includes all upcoming features.
The dev branch on this repository must always use the android-library dev branch.

### Release process

#### Stable Release

Stable releases are based on the git master.

1. Bump the version name and version code in the AndroidManifest.xml.
2. Create a release/tag in git. Tag name following the naming schema: Mayor.Minor (e.g. 1.2) naming the version number following the semantic versioning schema

#### Release Candidate Release

Release Candidate releases are based on the git master and are done between stable releases.

1. Bump the version name and version code in the AndroidManifest.xml, see below the version name and code concept.
2. Create a release/tag in git. Tag name following the naming schema: rc-Mayor.Minor-betaIncrement (e.g. rc-1.2-12) naming the version number following the semantic versioning schema

#### Development Release

Dev releases are based on the master branch and are done independently of stable releases for people willing to test new features and provide valuable feedback on new features to be incorporated before a feature gets released in the stable app.
