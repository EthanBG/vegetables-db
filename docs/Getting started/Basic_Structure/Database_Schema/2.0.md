## Database Schema

MyGarden needs a persistent storage space to store the different gardens created by the users.
Using [ionic-storage](https://github.com/ionic-team/ionic-storage) is a good solution, easy to use and a perfect integration with [Capacitor](https://capacitorjs.com/).

##### Garden

| keys             |             |                       |           |            | Description                                                                                                                                                    |
|------------------|-------------|-----------------------|-----------|------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 'gardenName' :   |             |                       |           |            | (<span class="text-blue">Garden</span>)                                                                                                                        |
|                  | description |                       |           |            | (<span class="text-blue">string</span>) A short description of the garden                                                                                      |
|                  | vegetables  |                       |           |            | (<span class="text-blue">[Vegetable.id](https://gitlab.com/m9712/mygarden/-/blob/master/src/app/shared/vegetable.model.ts)[ ]</span> Selected vegetable array) |
|                  | maps :      |                       |           |            | Object that contain the FabricJSON for each year an month                                                                                                      |
|                  |             | 'year'                |           |            | (<span class="text-blue">number</span>) The year                                                                                                               |
|                  |             |                       | 'month' : |            | (<span class="text-blue">number</span>) The month which contain the fabricJSON                                                                                 |
|                  | extra :     |                       |           |            | (<span class="text-blue">object</span>) Contain other information                                                                                              |
|                  |             | plantingMonth         |           |            | (<span class="text-blue">number[ ]</span>) Vegetable planting months                                                                                           |
|                  |             | databaseSchemaVersion |           |            | (<span class="text-blue">number</span>) Current database schema version                                                                                        |
| firstTime        |             |                       |           |            | (<span class="text-blue">boolean</span>) False if the app is not launch for the first time.                                                                    |
| languageSelected |             |                       |           |            | (<span class="text-blue">string</span>) The language selected by the user                                                                                      |

#### Breaking changes from 1.0

- Create the object month inside each year.
