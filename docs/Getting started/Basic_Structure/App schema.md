- **MyGarden App**

    * `src/`: Common part and the MyGarden app code
    * `package.json`: npm specific parts
    * `src/assets`: some resources (mostly images but also translation files) which are used in the project. Most of the are embedded to the code.
    * `src/app/services`: Angular services
    * `src/app/shared`: Angular model
    * `src/app/tab1`: First tab with vegetables overview
    * `src/app/tab2`: Second tab with Garden editor
    * `src/app/tab3`: Third tab with information about the app, authors...


- **MyGardenDB**

    * `mkdocs.yml`: Doc build configuration
    * `docs`: Docs folder
    * `docs/MyGardenDB`: vegetable data-base
    * `docs/assets`: Vegetable icons
    * `docs/stylesheets`: Custom css
    * `.gitlab/template/new-vegetable.md`: Vegetable template o
