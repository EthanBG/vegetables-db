# FR : Politique de confidentialité

Dernière mise à jour : 20 mai 2023

Cette politique de confidentialité décrit nos politiques et procédures sur la collecte, l'utilisation et la divulgation de vos informations lorsque vous utilisez le service et vous informe sur vos droits en matière de confidentialité et sur la manière dont la loi vous protège.

Nous utilisons vos données personnelles pour fournir et améliorer le service. En utilisant le Service, vous acceptez la collecte et l'utilisation d'informations conformément à la présente Politique de confidentialité. Cette politique de confidentialité a été créée avec l'aide du [générateur de politique de confidentialité](https://www.privacypolicies.com/privacy-policy-generator/).

# Interprétation et définitions

## Interprétation

Les mots dont la lettre initiale sont en majuscule ont des significations définies dans les conditions suivantes. Les définitions suivantes auront la même signification qu'elles apparaissent au singulier ou au pluriel.

## Définitions

Aux fins de la présente politique de confidentialité :

- __Compte__ désigne un compte unique créé pour vous permettre d'accéder à notre service ou à des parties de notre service.

- __Affilié__ désigne une entité qui contrôle, est contrôlée par où est sous contrôle commun avec une partie, où "contrôle" signifie la propriété de 50% ou plus des actions, participations ou autres titres habilités à voter pour l'élection des administrateurs ou d'autres dirigeants autorité.

- __Application__ désigne MyGarden, le logiciel fourni par la Société.

- __Entreprise__ (appelée soit "l'Entreprise", "Nous", "Notre" ou "Notre" dans le présent Contrat) fait référence à MyGarden.

- __Pays__ désigne : France

- __Appareil__ désigne tout appareil pouvant accéder au Service tel qu'un ordinateur, un téléphone portable ou une tablette numérique.


- Les __Données personnelles__ sont toutes les informations relatives à une personne identifiée ou identifiable.

- __Service__ désigne l'Application.

- __Prestataire__ désigne toute personne physique ou morale qui traite les données pour le compte de la Société. Il fait référence à des sociétés tierces ou à des personnes employées par la Société pour faciliter le Service, pour fournir le Service au nom de la Société, pour effectuer des services liés au Service ou pour aider la Société à analyser la manière dont le Service est utilisé.

- __Données d'utilisation__ fait référence aux données collectées automatiquement, soit générées par l'utilisation du Service, soit à partir de l'infrastructure du Service elle-même (par exemple, la durée d'une visite de page).

- __Vous__ désigne la personne accédant ou utilisant le Service, ou la société ou toute autre entité juridique au nom de laquelle cette personne accède ou utilise le Service, selon le cas.


# Collecte et utilisation de vos données personnelles

## Types de données collectées

### Données personnelles

Lors de l'utilisation de notre service, nous pouvons vous demander de nous fournir certaines informations personnellement identifiables qui peuvent être utilisées pour vous contacter ou vous identifier. Les informations personnellement identifiables peuvent inclure, mais sans s'y limiter :

- Des données d'utilisation

### Des données d'utilisation

Les données d'utilisation sont collectées automatiquement lors de l'utilisation du service.

Les données d'utilisation peuvent inclure des informations telles que l'adresse de protocole Internet de votre appareil (par exemple, l'adresse IP), le type de navigateur, la version du navigateur, les pages de notre service que vous visitez, l'heure et la date de votre visite, le temps passé sur ces pages, l'appareil unique identifiants et autres données de diagnostic.

Lorsque vous accédez au service via un appareil mobile, nous pouvons collecter certaines informations automatiquement, y compris, mais sans s'y limiter, le type d'appareil mobile que vous utilisez, l'identifiant unique de votre appareil mobile, l'adresse IP de votre appareil mobile, votre système d'exploitation, le type de navigateur Internet mobile que vous utilisez, les identifiants uniques de l'appareil et d'autres données de diagnostic.

Nous pouvons également collecter des informations que votre navigateur envoie chaque fois que vous visitez notre service ou lorsque vous accédez au service via un appareil mobile.

## Utilisation de vos données personnelles

La Société peut utiliser les Données Personnelles aux fins suivantes :

- __Pour fournir et maintenir notre Service__, y compris pour surveiller l'utilisation de notre Service.

- __Pour gérer Votre Compte :__ pour gérer Votre inscription en tant qu'utilisateur du Service. Les données personnelles que vous fournissez peuvent vous donner accès à différentes fonctionnalités du service qui sont à votre disposition en tant qu'utilisateur enregistré.

- __Pour l'exécution d'un contrat :__ l'élaboration, la conformité et la réalisation du contrat d'achat des produits, articles ou services que Vous avez achetés ou de tout autre contrat avec Nous par le biais du Service.

- __Pour vous contacter :__ pour vous contacter par e-mail, appels téléphoniques, SMS ou autres formes équivalentes de communication électronique, telles que les notifications push d'une application mobile concernant les mises à jour ou les communications informatives relatives aux fonctionnalités, produits ou services contractés, y compris la sécurité mises à jour, lorsque cela est nécessaire ou raisonnable pour leur mise en œuvre.

- __Pour vous fournir__ des actualités, des offres spéciales et des informations générales sur d'autres biens, services et événements que nous proposons et qui sont similaires à ceux que vous avez déjà achetés ou demandés, sauf si vous avez choisi de ne pas recevoir ces informations.

- __Pour gérer Vos demandes :__ Pour assister et gérer Vos demandes à Nous.

- __Pour les transferts d'entreprise :__ Nous pouvons utiliser Vos informations pour évaluer ou mener une fusion, une cession, une restructuration, une réorganisation, une dissolution ou toute autre vente ou transfert de tout ou partie de Nos actifs, que ce soit dans le cadre d'une entreprise en activité ou dans le cadre d'une faillite, liquidation ou procédure similaire, dans laquelle les données personnelles que nous détenons sur les utilisateurs de nos services font partie des actifs transférés.

- __À d'autres fins __: nous pouvons utiliser vos informations à d'autres fins, telles que l'analyse de données, l'identification des tendances d'utilisation, la détermination de l'efficacité de nos campagnes promotionnelles et pour évaluer et améliorer notre service, nos produits, nos services, notre marketing et votre expérience.


Nous pouvons partager vos informations personnelles dans les situations suivantes :

- __Avec des Prestataires de services:__ Nous pouvons partager Vos informations personnelles avec des Prestataires de services pour surveiller et analyser l'utilisation de notre Service, pour Vous contacter.
- __Pour les transferts d'entreprise:__ Nous pouvons partager ou transférer Vos informations personnelles dans le cadre de, ou pendant les négociations de, toute fusion, vente d'actifs de la Société, financement ou acquisition de tout ou partie de Notre entreprise à une autre société.
- __Avec des affiliés :__ Nous pouvons partager vos informations avec nos affiliés, auquel cas, nous demanderons à ces affiliés de respecter cette politique de confidentialité. Les sociétés affiliées incluent notre société mère et toutes autres filiales, partenaires de coentreprise ou autres sociétés que nous contrôlons ou qui sont sous contrôle commun avec nous.
- __Avec des partenaires commerciaux :__ Nous pouvons partager vos informations avec nos partenaires commerciaux pour vous proposer certains produits, services ou promotions.
- __Avec d'autres utilisateurs :__ lorsque vous partagez des informations personnelles ou interagissez autrement dans les zones publiques avec d'autres utilisateurs, ces informations peuvent être vues par tous les utilisateurs et peuvent être diffusées publiquement à l'extérieur.
- __Avec Votre consentement__ : Nous pouvons divulguer Vos informations personnelles à toute autre fin avec Votre consentement.

## Conservation de vos données personnelles

La Société ne conservera vos données personnelles que le temps nécessaire aux fins énoncées dans la présente politique de confidentialité. Nous conserverons et utiliserons vos données personnelles dans la mesure nécessaire pour nous conformer à nos obligations légales (par exemple, si nous sommes tenus de conserver vos données pour nous conformer aux lois applicables), résoudre les litiges et appliquer nos accords et politiques juridiques.

La Société conservera également les données d'utilisation à des fins d'analyse interne. Les données d'utilisation sont généralement conservées pendant une période plus courte, sauf lorsque ces données sont utilisées pour renforcer la sécurité ou pour améliorer la fonctionnalité de notre service, ou lorsque nous sommes légalement tenus de conserver ces données durant des périodes plus longues.

## Transfert de vos données personnelles

Vos informations, y compris les données personnelles, sont traitées dans les bureaux d'exploitation de la société et dans tout autre lieu où se trouvent les parties impliquées dans le traitement. Cela signifie que ces informations peuvent être transférées et conservées sur des ordinateurs situés en dehors de votre état, province, pays ou autre juridiction gouvernementale où les lois sur la protection des données peuvent différer de celles de votre juridiction.

Votre consentement à cette politique de confidentialité suivi de votre soumission de ces informations représente votre accord à ce transfert.

La société prendra toutes les mesures raisonnablement nécessaires pour garantir que vos données sont traitées en toute sécurité et conformément à la présente politique de confidentialité et aucun transfert de vos données personnelles n'aura lieu vers une organisation ou un pays à moins que des contrôles adéquats ne soient en place, y compris la sécurité de Vos données et autres informations personnelles.

## Supprimer vos données personnelles

Vous avez le droit de supprimer ou de demander que nous vous aidions à supprimer les données personnelles que nous avons collectées à votre sujet.

Notre service peut vous donner la possibilité de supprimer certaines informations vous concernant à partir du service.

Vous pouvez mettre à jour, modifier ou supprimer vos informations à tout moment en vous connectant à votre compte, si vous en avez un, et en visitant la section des paramètres du compte qui vous permet de gérer vos informations personnelles. Vous pouvez également nous contacter pour demander l'accès, la correction ou la suppression de toute information personnelle que vous nous avez fournie.

Veuillez noter, cependant, que nous pouvons avoir besoin de conserver certaines informations lorsque nous avons une obligation légale ou une base légale pour le faire.

## Divulgation de vos données personnelles

### Transactions commerciales

Si la Société est impliquée dans une fusion, une acquisition ou une vente d'actifs, Vos Données Personnelles peuvent être transférées. Nous vous aviserons avant que vos données personnelles ne soient transférées et soumises à une politique de confidentialité différente.

### Forces de l'ordre

Dans certaines circonstances, la Société peut être tenue de divulguer vos données personnelles si la loi l'exige ou en réponse à des demandes valables d'autorités publiques (par exemple, un tribunal ou une agence gouvernementale).

### Autres exigences légales

La Société peut divulguer vos données personnelles en croyant de bonne foi qu'une telle action est nécessaire pour :

- Respecter une obligation légale
- Protéger et défendre les droits ou les biens de la Société
- Prévenir ou enquêter sur d'éventuels actes répréhensibles en rapport avec le Service
- Protéger la sécurité personnelle des Utilisateurs du Service ou du public
- Protéger contre la responsabilité légale

## Sécurité de vos données personnelles

La sécurité de vos données personnelles est importante pour nous, mais rappelez-vous qu'aucune méthode de transmission sur Internet ou méthode de stockage électronique n'est sécurisée à 100 %. Bien que nous nous efforcions d'utiliser des moyens commercialement acceptables pour protéger vos données personnelles, nous ne pouvons garantir leur sécurité absolue.

# Confidentialité des enfants

Notre service ne s'adresse pas aux personnes de moins de 13 ans. Nous ne collectons pas sciemment d'informations personnellement identifiables auprès de personnes de moins de 13 ans. Si vous êtes un parent ou un tuteur et que vous savez que votre enfant nous a fourni des données personnelles, veuillez Contactez-nous. Si nous apprenons que nous avons collecté des données personnelles auprès d'une personne de moins de 13 ans sans vérification du consentement parental, nous prenons des mesures pour supprimer ces informations de nos serveurs.

Si nous devons compter sur le consentement comme base légale pour le traitement de vos informations et que votre pays exige le consentement d'un parent, nous pouvons exiger le consentement de votre parent avant de collecter et d'utiliser ces informations.

# Liens vers d'autres sites Web

Notre service peut contenir des liens vers d'autres sites Web qui ne sont pas exploités par nous. Si vous cliquez sur un lien tiers, vous serez dirigé vers le site de ce tiers. Nous vous conseillons vivement de consulter la politique de confidentialité de chaque site que vous visitez.

Nous n'avons aucun contrôle et n'assumons aucune responsabilité quant au contenu, aux politiques de confidentialité ou aux pratiques des sites ou services tiers.

# Modifications de cette politique de confidentialité

Nous pouvons mettre à jour notre politique de confidentialité de temps à autre. Nous vous informerons de tout changement en publiant la nouvelle politique de confidentialité sur cette page.

Nous vous informerons par e-mail et/ou par un avis visible sur notre service, avant que le changement ne devienne effectif et mettrons à jour la date de « dernière mise à jour » en haut de cette politique de confidentialité.

Il vous est conseillé de consulter périodiquement cette politique de confidentialité pour tout changement. Les modifications apportées à cette politique de confidentialité entrent en vigueur lorsqu'elles sont publiées sur cette page.

# Contactez-nous

Si vous avez des questions concernant cette politique de confidentialité, vous pouvez nous contacter :

- Par email : BugProgEnterprise@gmail.com
